from bittrex import Bittrex, API_V2_0
from matplotlib.finance import candlestick2_ohlc
import matplotlib.pyplot as plt
from indicators import Indicators
import bot

MA_PERIOD = 50
RSI_PERIOD = 14

bi = Bittrex(None, None, api_version=API_V2_0) 
market_sums = bi.get_market_summaries()['result']
markets = map(lambda m: m['Market']['MarketName'], market_sums)

print markets

def check(bi, market):
	candles = bi.get_candles(market, 'day')['result']
	# opens = map(lambda c: c['O'], candles)
	# highs = map(lambda c: c['H'], candles)
	# lows = map(lambda c: c['L'], candles)
	closes = map(lambda c: c['L'], candles)

	ind = Indicators()
	v = ind.RSI(closes, RSI_PERIOD)
	print v
	return v < 33

	bot.send_new_posts(candles, v)


for marketName in markets:
	print marketName, ('YES' if check(bi, marketName) else 'NO')

